namespace CritterGame.Business
{
    public interface IAwakable
    {
        void Awake();
    }
}