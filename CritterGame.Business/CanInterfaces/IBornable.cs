namespace CritterGame.Business
{
    public interface IBornable
    {
        void Born(string name);
    }
}