﻿namespace CritterGame.Business
{
    public interface IFlyable
    {
        string Fly(int statusModifier);
    }
}