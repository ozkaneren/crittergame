﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CritterGame.Business.CanInterfaces
{
    public interface IRunnable
    {
        string Run(int statusModifier);
    }
}
