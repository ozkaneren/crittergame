﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CritterGame.Business
{
    public interface ITalkable
    {
        string Talk(int talkSubject);
    }
}
