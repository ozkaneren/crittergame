namespace CritterGame.Business
{
    public enum HungerStatus
    {
        Dying,
        Starving,
        Normal,
        Full,
    }
}