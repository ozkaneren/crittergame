﻿using CritterGame.Business.Constants;

namespace CritterGame.Business.Extensions
{
    public static class StatusCounterExtension
    {
        public static int ToSafe(this int number)
        {
            if (number < BusinessConstants.MinStatusCounter)
            {
                return BusinessConstants.MinStatusCounter;
            }

            if (number > BusinessConstants.MaxtatusCounter)
            {
                return BusinessConstants.MaxtatusCounter;
            }

            return number;
        }
    }
}
