﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CritterGame.Business.Factory
{
    public static class CritterFactory
    {
        public static Critter CreateCritter(string name, string critterType)
        {
            Critter critter;

            if (critterType.Equals("Dog"))
            {
                critter = new Dog(name);
            }
            else if (critterType.Equals("Fish"))
            {
                critter = new Fish(name);
            }
            else if (critterType.Equals("Pigeon"))
            {
                critter = new Pigeon(name);
            }
            else
            {
                throw new Exception("Invalid Critter Type");
            }

            return critter;
        }
    }
}
