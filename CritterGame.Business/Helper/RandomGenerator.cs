﻿using System;

namespace CritterGame.Business.Helper
{
    public class RandomGenerator
    {
        private static RandomGenerator _randomGenerator;
        private readonly Random _random;


        private RandomGenerator()
        {
            _random = new Random();
        }

        public static RandomGenerator Instance
        {
            get
            {
                if (_randomGenerator == null)
                {
                    _randomGenerator = new RandomGenerator();
                }

                return _randomGenerator;
            }
        }

        public int GenerateRandomNumber(int from, int to)
        {
            return _random.Next(from, to);
        }
    }
}
