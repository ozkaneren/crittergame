﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using CritterGame.Business.CanInterfaces;
using CritterGame.Business.Extensions;

namespace CritterGame.Business
{
    public abstract class Critter : IBornable, ITalkable, IEatable, IPlayeble, ISleepible, IAgeable, IFriendable
    {
        protected int MoodCounter;
        protected int EatCounter;
        protected int SleepCounter;
        protected bool IsCritterDied;


        public string Type { get; protected set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public Critter Friend { get; protected set; }

        public Critter Lover { get; protected set; }

        protected Critter(string name)
        {
            this.Born(name);
        }

        public MoodStatus CurrentMood
        {
            get
            {
                if (MoodCounter >= 14)
                {
                    return MoodStatus.Angry;
                }
                else if (MoodCounter <= 13 && MoodCounter >= 7)
                {
                    return MoodStatus.Melancholy;
                }
                else if (MoodCounter <= 7 && MoodCounter >= 3)
                {
                    return MoodStatus.Happpy;
                }
                else
                {
                    return MoodStatus.LovesYou;
                }
            }
        }
        public SleepStatus SleepStatus
        {
            get
            {
                if (SleepCounter >= 14)
                {
                    return SleepStatus.Slepless;
                }
                else if (SleepCounter <= 13 && SleepCounter >= 7)
                {
                    return SleepStatus.Tired;
                }
                else
                {
                    return SleepStatus.Awake;
                }
            }
        }

        public HungerStatus HungerStatus
        {
            get
            {
                if (EatCounter >= 14)
                {
                    return HungerStatus.Dying;
                }
                else if (EatCounter <= 13 && EatCounter >= 7)
                {
                    return HungerStatus.Starving;
                }
                else if (EatCounter <= 7 && EatCounter >= 4)
                {
                    return HungerStatus.Normal;
                }
                else
                {
                    return HungerStatus.Full;
                }
            }
        }


        public void Born(string name)
        {
            EatCounter = 10;
            MoodCounter = 10;
            SleepCounter = 10;
            IsCritterDied = false;
            this.Age = 0;
            this.Name = name;
        }

        public void Die()
        {
            IsCritterDied = true;

        }

        public string Talk(int talkSubject)
        {
            string response = string.Empty;

            switch (talkSubject)
            {
                case 0:
                    response = "";
                    MoodCounter = MoodCounter - 5;
                    break;
                case 1:
                    response = "";
                    MoodCounter = MoodCounter - 6;
                    break;
                case 2:
                    response = "";
                    MoodCounter = MoodCounter - 7;
                    break;
                default:
                    MoodCounter = MoodCounter - (-4);
                    response = "";
                    break;
            }

            return response;
        }

        public void Eat(int eatCounter)
        {
            if (EatCounter > 0 && eatCounter <= 14)
            {
                EatCounter = (EatCounter - eatCounter).ToSafe();

            }
            if (SleepCounter > 0 && SleepCounter <= 14)
            {
                SleepCounter = (SleepCounter + eatCounter).ToSafe();

            }
            if (MoodCounter > 0 && MoodCounter <= 14)
            {
                MoodCounter = (MoodCounter - eatCounter).ToSafe();

            }
        }

        public void Play(int playNumber)
        {
            if (MoodCounter > 0 && MoodCounter <= 14)
            {
                MoodCounter = (MoodCounter - playNumber).ToSafe();
            }
            if (SleepCounter > 0 && SleepCounter <= 14)
            {
                SleepCounter = (SleepCounter + playNumber).ToSafe();
            }
            if (EatCounter > 0 && EatCounter <= 14)
            {
                EatCounter = (EatCounter + EatCounter).ToSafe();

            }
        }

        public void Sleep(int counterNumber)
        {
            if (SleepCounter > 0 && SleepCounter <= 14)
            {
                SleepCounter = (SleepCounter - counterNumber).ToSafe();
            }
            if (MoodCounter > 0 && MoodCounter <= 14)
            {
                MoodCounter = (MoodCounter - counterNumber).ToSafe();
            }
            if (EatCounter > 0 && EatCounter <= 14)
            {
                this.EatCounter = (this.EatCounter + EatCounter).ToSafe();
            }
            
        }

      
        
        public void GrowOld()
        {

            EatCounter = (EatCounter + 1).ToSafe(); 
            MoodCounter = (MoodCounter +1).ToSafe();
            SleepCounter = (SleepCounter + 1).ToSafe();

            Age++;

            if (Age == 365)
            {
                this.Die();
            }
        }


        public abstract Critter FindFriend();


    }
}
