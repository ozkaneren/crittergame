﻿using CritterGame.Business.CanInterfaces;
using CritterGame.Business.Helper;

namespace CritterGame.Business
{
    public class Dog : Critter, IRunnable
    {
        public Dog(string name) : base(name)
        {
            this.Type = "Dog";
            this.MoodCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.EatCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.SleepCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
        }

        public string Run(int statusModifier)
        {
            EatCounter = EatCounter - statusModifier;
            MoodCounter = MoodCounter - statusModifier;
            SleepCounter = SleepCounter - statusModifier;
            return "koş";

        }

        public override Critter FindFriend()
        {
            this.Friend = new Dog("Eevee");

            return this.Friend;
        }
    }
}
