﻿using CritterGame.Business.CanInterfaces;
using CritterGame.Business.Helper;


namespace CritterGame.Business
{
    public class Fish : Critter,ISwimmable
    {
        public Fish(string name) : base(name)
        {
            this.Type = "Fish";
            this.MoodCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.EatCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.SleepCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
        }

        public string Swim(int statusModifier)
        {
            EatCounter = EatCounter - statusModifier;
            MoodCounter = MoodCounter - statusModifier;
            SleepCounter = SleepCounter - statusModifier;
            return "yüz";
        }

        public override Critter FindFriend()
        {
            this.Friend = new Fish("Horse Sea");

            return this.Friend;
        }
    }
}
