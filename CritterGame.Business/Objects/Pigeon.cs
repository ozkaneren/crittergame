﻿using CritterGame.Business.Helper;

namespace CritterGame.Business
{
    public class Pigeon : Critter, IFlyable
    {
        public Pigeon(string name) : base(name)
        {
            this.Type = "Pigeon";
            this.MoodCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.EatCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
            this.SleepCounter = RandomGenerator.Instance.GenerateRandomNumber(1, 14);
        }


        public string Fly(int statusModifier)
        {
            EatCounter = EatCounter - statusModifier;
            MoodCounter = MoodCounter - statusModifier;
            SleepCounter = SleepCounter - statusModifier;
            return "uç";
        }

        public override Critter FindFriend()
        {
            this.Friend = new Pigeon("Articuno");

            return this.Friend;
        }
    }
}
