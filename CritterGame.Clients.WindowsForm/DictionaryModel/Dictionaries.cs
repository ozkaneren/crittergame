﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CritterGame.Business;

namespace CritterGame.Clients.WindowsForm.DictionaryModel
{
    public static class Dictionaries
    {
        public static Dictionary<string, string> CritterImages => new Dictionary<string, string>()
        {
            { "Dog",@"C:\Users\ozkan.eren\Desktop\Gorseller\dog.png" },
            { "Fish", @"C:\Users\ozkan.eren\Desktop\Gorseller\fish.png" },
            { "Pigeon", @"C:\Users\ozkan.eren\Desktop\Gorseller\pigeon.png" }
            //{ "Dog", @"C:\Users\ozkan.eren\Desktop\Gifs\dogg.gif" },
            //{ "Fish", @"C:\Users\ozkan.eren\Desktop\Gifs\fish.gif" },
            //{ "Pigeon", @"C:\Users\ozkan.eren\Desktop\Gifs\pigeon.gif" }
        };
        public static Dictionary<string, string> CritterImagesFriend => new Dictionary<string, string>()
        {
            { "Dog",@"C:\Users\ozkan.eren\Desktop\Gorseller\dog2.png" },
            { "Fish", @"C:\Users\ozkan.eren\Desktop\Gorseller\fish2.png" },
            { "Pigeon", @"C:\Users\ozkan.eren\Desktop\Gorseller\pigeon2.png" }
            //{ "Dog", @"C:\Users\ozkan.eren\Desktop\Gifs\dogg.gif" },
            //{ "Fish", @"C:\Users\ozkan.eren\Desktop\Gifs\fish.gif" },
            //{ "Pigeon", @"C:\Users\ozkan.eren\Desktop\Gifs\pigeon.gif" }
        };

        public static Dictionary<MoodStatus, string> MoodImages
        {
            get
            {
                return new Dictionary<MoodStatus, string>()
                {
                    { MoodStatus.Happpy, @"C:\Users\ozkan.eren\Desktop\Gorseller\Smile.png" },
                    { MoodStatus.Angry,  @"C:\Users\ozkan.eren\Desktop\Gorseller\Sad.png" },
                    { MoodStatus.LovesYou,  @"C:\Users\ozkan.eren\Desktop\Gorseller\LovesYou.png"},
                    { MoodStatus.Melancholy,  @"C:\Users\ozkan.eren\Desktop\Gorseller\Normal.png"}
                };
            }
        }

        public static Dictionary<HungerStatus, String> EatImages
        {
            get
            {
                return new Dictionary<HungerStatus, string>()
                {
                    {HungerStatus.Dying,  @"C:\Users\ozkan.eren\Desktop\Gorseller\Hunger.png"},
                    {HungerStatus.Full, @"C:\Users\ozkan.eren\Desktop\Gorseller\Full.png"},
                    {HungerStatus.Normal, @"C:\Users\ozkan.eren\Desktop\Gorseller\Full.png"},
                    {HungerStatus.Starving, @"C:\Users\ozkan.eren\Desktop\Gorseller\Hunger.png"}
                };
            }
        }

        public static Dictionary<SleepStatus, String> SleepImages
        {
            get
            {
                return new Dictionary<SleepStatus, string>()
                {
                    {SleepStatus.Awake, @"C:\Users\ozkan.eren\Desktop\Gorseller\Awake.png"},
                    {SleepStatus.Slepless, @"C:\Users\ozkan.eren\Desktop\Gorseller\Slepless.png"},
                    {SleepStatus.Tired, @"C:\Users\ozkan.eren\Desktop\Gorseller\Tired.png"}
                };
            }
        }

        public static Dictionary<string, string> Conversation
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    {"Naber ?", ""},
                    {"Kimsin sen ?", "Ben senin critterınım"},
                    {"Acıktın mı ?", ""},
                    {"Hiç beğenemedim seni..", ":("}
                };
            }
        }
        public static Dictionary<string, int> ConversationStatusNumFinder
        {
            get
            {
                return new Dictionary<string, int>()
                {
                    {"Naber ?", 2},
                    {"Kimsin sen ?", 2},
                    {"Acıktın mı ?", 4},
                    {"Hiç beğenemedim seni..", -5}
                };
            }
        }
        public static Dictionary<string, string> StatusTranslater
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    {"Happpy", "mutluyum :)" },
                    {"Angry", "moralim çok bozuk :(" },
                    {"LovesYou", "seni çok seviyorum :)"},
                    {"Melancholy", "moralim bozuk" },
                    {"Dying", "açlıktan ölüyorum"},
                    {"Full", "tokum"},
                    {"Normal", "aç değilim ama yiyebilirim :P" },
                    {"Starving", "çok açım"},
                    {"Awake", "uykum yok"},
                    {"Slepless", "çok uykum var"},
                    {"Tired", "yorgunum"}
                };
            }


        }



        public static Dictionary<int, string> RandomConversation
        {
            get
            {
                return new Dictionary<int, string>()
                {
                    {1, "Bilmem, bana sorma"},
                    {2, "Ot yemeyi sevmem ben"},
                    {3, "Yaşlanıyorum gittikçe :("},
                    {4, "Yanında gördüğüm o yaratıkda kimdi"},
                    {5, "Büyüyünce ayrı eve çıkıcam ben"},
                    {6, "Neden bir arkadaşım yok"},
                    {7, "Kim bilir belkide legathon' un dünyaya \n çarpmasına 20 gün vardır"},
                    {8, "Neden böyle düşündün ki analayamadım.\n Hatta bir şey mi düşündün onu bile anlayamadım :("}
                   
                };
            }
        }
        public static Dictionary<int, bool> RandomConMeanings
        {
            get
            {
                return new Dictionary<int, bool>()
                {
                    {6, true},
                    {9, true}
                };
            }
        }

        public static Dictionary<Type, int> TypeDeterminate
        {
            get
            {
                return new Dictionary<Type, int>()
                {
                    {typeof(Dog), 1},
                    {typeof(Fish), 2},
                    {typeof(Pigeon), 3}
                };
            }
        }

        public static Dictionary<Type, string> Feature
        {
            get
            {
                return new Dictionary<Type, string>()
                {
                    {typeof(Dog), "Run"},
                    {typeof(Fish), "Swim"},
                    {typeof(Pigeon), "Fly"}
                };
            }
        }
    }
}

