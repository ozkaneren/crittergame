﻿namespace CritterGame.Clients.WindowsForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.plDiyalog = new System.Windows.Forms.Panel();
            this.lbTalkHistory = new System.Windows.Forms.ListBox();
            this.cbConversation = new System.Windows.Forms.ComboBox();
            this.Talk_List = new System.Windows.Forms.Label();
            this.p15 = new System.Windows.Forms.PictureBox();
            this.plUyku = new System.Windows.Forms.Panel();
            this.p13 = new System.Windows.Forms.PictureBox();
            this.combo_Sleep = new System.Windows.Forms.ComboBox();
            this.PLYemek = new System.Windows.Forms.Panel();
            this.btnYemekList = new System.Windows.Forms.Button();
            this.pnlYemekList = new System.Windows.Forms.Panel();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.p14 = new System.Windows.Forms.PictureBox();
            this.pnlPlayList = new System.Windows.Forms.Panel();
            this.check_c = new System.Windows.Forms.RadioButton();
            this.check_a = new System.Windows.Forms.RadioButton();
            this.check_b = new System.Windows.Forms.RadioButton();
            this.check_d = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pnlCritterInfo = new System.Windows.Forms.Panel();
            this.pnlBornCritter = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pbDog = new System.Windows.Forms.PictureBox();
            this.btnBornCritter = new System.Windows.Forms.Button();
            this.pbFish = new System.Windows.Forms.PictureBox();
            this.pbPigeon = new System.Windows.Forms.PictureBox();
            this.txtCritterName = new System.Windows.Forms.TextBox();
            this.pbCritterImages = new System.Windows.Forms.PictureBox();
            this.lblCritterName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCritter = new System.Windows.Forms.Label();
            this.j = new System.Windows.Forms.PictureBox();
            this.k = new System.Windows.Forms.PictureBox();
            this.pbMood = new System.Windows.Forms.PictureBox();
            this.pbHunger = new System.Windows.Forms.PictureBox();
            this.i = new System.Windows.Forms.PictureBox();
            this.pbSleep = new System.Windows.Forms.PictureBox();
            this.h = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.d = new System.Windows.Forms.PictureBox();
            this.g = new System.Windows.Forms.PictureBox();
            this.e = new System.Windows.Forms.PictureBox();
            this.f = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.plPlay = new System.Windows.Forms.Panel();
            this.btnOyunList = new System.Windows.Forms.Button();
            this.p16 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnlFriend = new System.Windows.Forms.Panel();
            this.lblCritterFriend = new System.Windows.Forms.Label();
            this.btnSpecial2 = new System.Windows.Forms.Button();
            this.btnSpecial = new System.Windows.Forms.Button();
            this.pbCritterFriend = new System.Windows.Forms.PictureBox();
            this.plDiyalog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).BeginInit();
            this.plUyku.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).BeginInit();
            this.PLYemek.SuspendLayout();
            this.pnlYemekList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).BeginInit();
            this.pnlPlayList.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlCritterInfo.SuspendLayout();
            this.pnlBornCritter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPigeon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCritterImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.j)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.k)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHunger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.i)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSleep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.h)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.g)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.f)).BeginInit();
            this.plPlay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).BeginInit();
            this.pnlFriend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCritterFriend)).BeginInit();
            this.SuspendLayout();
            // 
            // plDiyalog
            // 
            this.plDiyalog.BackColor = System.Drawing.Color.Aquamarine;
            this.plDiyalog.Controls.Add(this.lbTalkHistory);
            this.plDiyalog.Controls.Add(this.cbConversation);
            this.plDiyalog.Controls.Add(this.Talk_List);
            this.plDiyalog.Controls.Add(this.p15);
            this.plDiyalog.Location = new System.Drawing.Point(0, -1);
            this.plDiyalog.Name = "plDiyalog";
            this.plDiyalog.Size = new System.Drawing.Size(261, 353);
            this.plDiyalog.TabIndex = 68;
            // 
            // lbTalkHistory
            // 
            this.lbTalkHistory.FormattingEnabled = true;
            this.lbTalkHistory.Location = new System.Drawing.Point(3, 66);
            this.lbTalkHistory.Name = "lbTalkHistory";
            this.lbTalkHistory.Size = new System.Drawing.Size(252, 186);
            this.lbTalkHistory.TabIndex = 51;
            // 
            // cbConversation
            // 
            this.cbConversation.FormattingEnabled = true;
            this.cbConversation.Items.AddRange(new object[] {
            "Naber ?",
            "Kimsin sen ?",
            "Acıktın mı ?",
            "Hiç beğenemedim seni.."});
            this.cbConversation.Location = new System.Drawing.Point(54, 267);
            this.cbConversation.Name = "cbConversation";
            this.cbConversation.Size = new System.Drawing.Size(142, 21);
            this.cbConversation.TabIndex = 47;
            this.cbConversation.Text = "Diyaloglar";
            this.cbConversation.UseWaitCursor = true;
            this.cbConversation.SelectedIndexChanged += new System.EventHandler(this.combo_Talk_SelectedIndexChanged);
            // 
            // Talk_List
            // 
            this.Talk_List.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Talk_List.Location = new System.Drawing.Point(96, 36);
            this.Talk_List.Name = "Talk_List";
            this.Talk_List.Size = new System.Drawing.Size(100, 23);
            this.Talk_List.TabIndex = 46;
            this.Talk_List.Text = "Diyaloglar";
            this.Talk_List.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Talk_List.UseWaitCursor = true;
            // 
            // p15
            // 
            this.p15.Image = ((System.Drawing.Image)(resources.GetObject("p15.Image")));
            this.p15.InitialImage = ((System.Drawing.Image)(resources.GetObject("p15.InitialImage")));
            this.p15.Location = new System.Drawing.Point(54, 35);
            this.p15.Name = "p15";
            this.p15.Size = new System.Drawing.Size(25, 25);
            this.p15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p15.TabIndex = 50;
            this.p15.TabStop = false;
            this.p15.UseWaitCursor = true;
            // 
            // plUyku
            // 
            this.plUyku.BackColor = System.Drawing.Color.LavenderBlush;
            this.plUyku.Controls.Add(this.p13);
            this.plUyku.Controls.Add(this.combo_Sleep);
            this.plUyku.Location = new System.Drawing.Point(788, 352);
            this.plUyku.Name = "plUyku";
            this.plUyku.Size = new System.Drawing.Size(261, 353);
            this.plUyku.TabIndex = 67;
            // 
            // p13
            // 
            this.p13.Image = ((System.Drawing.Image)(resources.GetObject("p13.Image")));
            this.p13.InitialImage = ((System.Drawing.Image)(resources.GetObject("p13.InitialImage")));
            this.p13.Location = new System.Drawing.Point(61, 132);
            this.p13.Name = "p13";
            this.p13.Size = new System.Drawing.Size(25, 25);
            this.p13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p13.TabIndex = 48;
            this.p13.TabStop = false;
            this.p13.UseWaitCursor = true;
            // 
            // combo_Sleep
            // 
            this.combo_Sleep.FormattingEnabled = true;
            this.combo_Sleep.Items.AddRange(new object[] {
            "3",
            "4",
            "6",
            "8",
            "10"});
            this.combo_Sleep.Location = new System.Drawing.Point(91, 134);
            this.combo_Sleep.Name = "combo_Sleep";
            this.combo_Sleep.Size = new System.Drawing.Size(121, 21);
            this.combo_Sleep.TabIndex = 43;
            this.combo_Sleep.Text = "Uyku Saatleri";
            this.combo_Sleep.UseWaitCursor = true;
            this.combo_Sleep.SelectedIndexChanged += new System.EventHandler(this.combo_Sleep_SelectedIndexChanged);
            // 
            // PLYemek
            // 
            this.PLYemek.BackColor = System.Drawing.Color.HotPink;
            this.PLYemek.Controls.Add(this.btnYemekList);
            this.PLYemek.Controls.Add(this.pnlYemekList);
            this.PLYemek.Controls.Add(this.label3);
            this.PLYemek.Controls.Add(this.p14);
            this.PLYemek.Location = new System.Drawing.Point(788, -1);
            this.PLYemek.Name = "PLYemek";
            this.PLYemek.Size = new System.Drawing.Size(261, 353);
            this.PLYemek.TabIndex = 66;
            // 
            // btnYemekList
            // 
            this.btnYemekList.Location = new System.Drawing.Point(91, 267);
            this.btnYemekList.Name = "btnYemekList";
            this.btnYemekList.Size = new System.Drawing.Size(75, 23);
            this.btnYemekList.TabIndex = 21;
            this.btnYemekList.Text = "Ye !!";
            this.btnYemekList.UseVisualStyleBackColor = true;
            this.btnYemekList.Click += new System.EventHandler(this.btnYemekList_Click);
            // 
            // pnlYemekList
            // 
            this.pnlYemekList.Controls.Add(this.checkBox6);
            this.pnlYemekList.Controls.Add(this.checkBox2);
            this.pnlYemekList.Controls.Add(this.checkBox3);
            this.pnlYemekList.Controls.Add(this.checkBox4);
            this.pnlYemekList.Controls.Add(this.checkBox5);
            this.pnlYemekList.Location = new System.Drawing.Point(61, 134);
            this.pnlYemekList.Name = "pnlYemekList";
            this.pnlYemekList.Size = new System.Drawing.Size(139, 124);
            this.pnlYemekList.TabIndex = 53;
            this.pnlYemekList.UseWaitCursor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(3, 95);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(77, 17);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "Karnıbahar";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.UseWaitCursor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(3, 3);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(78, 17);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "Hamburger";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.UseWaitCursor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(3, 26);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(55, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "Sosisli";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.UseWaitCursor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(3, 49);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(112, 17);
            this.checkBox4.TabIndex = 6;
            this.checkBox4.Text = "Patates Kızartması";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.UseWaitCursor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(3, 72);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(75, 17);
            this.checkBox5.TabIndex = 1;
            this.checkBox5.Text = "Dondurma";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.UseWaitCursor = true;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(88, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 23);
            this.label3.TabIndex = 45;
            this.label3.Text = "Yemekler";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.UseWaitCursor = true;
            // 
            // p14
            // 
            this.p14.Image = ((System.Drawing.Image)(resources.GetObject("p14.Image")));
            this.p14.InitialImage = ((System.Drawing.Image)(resources.GetObject("p14.InitialImage")));
            this.p14.Location = new System.Drawing.Point(59, 102);
            this.p14.Name = "p14";
            this.p14.Size = new System.Drawing.Size(25, 25);
            this.p14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p14.TabIndex = 49;
            this.p14.TabStop = false;
            this.p14.UseWaitCursor = true;
            // 
            // pnlPlayList
            // 
            this.pnlPlayList.Controls.Add(this.check_c);
            this.pnlPlayList.Controls.Add(this.check_a);
            this.pnlPlayList.Controls.Add(this.check_b);
            this.pnlPlayList.Controls.Add(this.check_d);
            this.pnlPlayList.Location = new System.Drawing.Point(51, 114);
            this.pnlPlayList.Name = "pnlPlayList";
            this.pnlPlayList.Size = new System.Drawing.Size(140, 97);
            this.pnlPlayList.TabIndex = 65;
            this.pnlPlayList.UseWaitCursor = true;
            // 
            // check_c
            // 
            this.check_c.AutoSize = true;
            this.check_c.Location = new System.Drawing.Point(3, 48);
            this.check_c.Name = "check_c";
            this.check_c.Size = new System.Drawing.Size(60, 17);
            this.check_c.TabIndex = 3;
            this.check_c.Text = "Triatlon";
            this.check_c.UseVisualStyleBackColor = true;
            this.check_c.UseWaitCursor = true;
            // 
            // check_a
            // 
            this.check_a.AutoSize = true;
            this.check_a.Location = new System.Drawing.Point(3, 1);
            this.check_a.Name = "check_a";
            this.check_a.Size = new System.Drawing.Size(79, 17);
            this.check_a.TabIndex = 3;
            this.check_a.Text = "Buz Hokeyi";
            this.check_a.UseVisualStyleBackColor = true;
            this.check_a.UseWaitCursor = true;
            // 
            // check_b
            // 
            this.check_b.AutoSize = true;
            this.check_b.Location = new System.Drawing.Point(3, 24);
            this.check_b.Name = "check_b";
            this.check_b.Size = new System.Drawing.Size(53, 17);
            this.check_b.TabIndex = 2;
            this.check_b.Text = "Ragbi";
            this.check_b.UseVisualStyleBackColor = true;
            this.check_b.UseWaitCursor = true;
            // 
            // check_d
            // 
            this.check_d.AutoSize = true;
            this.check_d.Location = new System.Drawing.Point(3, 71);
            this.check_d.Name = "check_d";
            this.check_d.Size = new System.Drawing.Size(109, 17);
            this.check_d.TabIndex = 5;
            this.check_d.Text = "Modern Pentatlon";
            this.check_d.UseVisualStyleBackColor = true;
            this.check_d.UseWaitCursor = true;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(82, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 64;
            this.label5.Text = "Oyun Listesi";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.UseWaitCursor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Cornsilk;
            this.panel9.Controls.Add(this.pnlCritterInfo);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.lblCritter);
            this.panel9.Controls.Add(this.j);
            this.panel9.Controls.Add(this.k);
            this.panel9.Controls.Add(this.pbMood);
            this.panel9.Controls.Add(this.pbHunger);
            this.panel9.Controls.Add(this.i);
            this.panel9.Controls.Add(this.pbSleep);
            this.panel9.Controls.Add(this.h);
            this.panel9.Controls.Add(this.label4);
            this.panel9.Controls.Add(this.d);
            this.panel9.Controls.Add(this.g);
            this.panel9.Controls.Add(this.e);
            this.panel9.Controls.Add(this.f);
            this.panel9.Controls.Add(this.progressBar1);
            this.panel9.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.panel9.Location = new System.Drawing.Point(261, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(530, 705);
            this.panel9.TabIndex = 63;
            this.panel9.UseWaitCursor = true;
            // 
            // pnlCritterInfo
            // 
            this.pnlCritterInfo.Controls.Add(this.pnlBornCritter);
            this.pnlCritterInfo.Controls.Add(this.pbCritterImages);
            this.pnlCritterInfo.Controls.Add(this.lblCritterName);
            this.pnlCritterInfo.Location = new System.Drawing.Point(6, 189);
            this.pnlCritterInfo.Name = "pnlCritterInfo";
            this.pnlCritterInfo.Size = new System.Drawing.Size(507, 352);
            this.pnlCritterInfo.TabIndex = 71;
            this.pnlCritterInfo.UseWaitCursor = true;
            // 
            // pnlBornCritter
            // 
            this.pnlBornCritter.Controls.Add(this.label2);
            this.pnlBornCritter.Controls.Add(this.pbDog);
            this.pnlBornCritter.Controls.Add(this.btnBornCritter);
            this.pnlBornCritter.Controls.Add(this.pbFish);
            this.pnlBornCritter.Controls.Add(this.pbPigeon);
            this.pnlBornCritter.Controls.Add(this.txtCritterName);
            this.pnlBornCritter.Location = new System.Drawing.Point(1, 0);
            this.pnlBornCritter.Name = "pnlBornCritter";
            this.pnlBornCritter.Size = new System.Drawing.Size(501, 365);
            this.pnlBornCritter.TabIndex = 45;
            this.pnlBornCritter.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(236, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 19);
            this.label2.TabIndex = 46;
            this.label2.Text = "Adım";
            this.label2.UseWaitCursor = true;
            // 
            // pbDog
            // 
            this.pbDog.Location = new System.Drawing.Point(12, 121);
            this.pbDog.Name = "pbDog";
            this.pbDog.Size = new System.Drawing.Size(140, 140);
            this.pbDog.TabIndex = 43;
            this.pbDog.TabStop = false;
            this.pbDog.UseWaitCursor = true;
            this.pbDog.Click += new System.EventHandler(this.pbDog_Click);
            // 
            // btnBornCritter
            // 
            this.btnBornCritter.Location = new System.Drawing.Point(156, 300);
            this.btnBornCritter.Name = "btnBornCritter";
            this.btnBornCritter.Size = new System.Drawing.Size(183, 23);
            this.btnBornCritter.TabIndex = 54;
            this.btnBornCritter.Text = "Doğ";
            this.btnBornCritter.UseVisualStyleBackColor = true;
            this.btnBornCritter.UseWaitCursor = true;
            this.btnBornCritter.Click += new System.EventHandler(this.btnBornCritter_Click);
            // 
            // pbFish
            // 
            this.pbFish.Location = new System.Drawing.Point(179, 120);
            this.pbFish.Name = "pbFish";
            this.pbFish.Size = new System.Drawing.Size(140, 140);
            this.pbFish.TabIndex = 43;
            this.pbFish.TabStop = false;
            this.pbFish.UseWaitCursor = true;
            this.pbFish.Click += new System.EventHandler(this.pbFish_Click);
            // 
            // pbPigeon
            // 
            this.pbPigeon.Location = new System.Drawing.Point(355, 121);
            this.pbPigeon.Name = "pbPigeon";
            this.pbPigeon.Size = new System.Drawing.Size(140, 140);
            this.pbPigeon.TabIndex = 43;
            this.pbPigeon.TabStop = false;
            this.pbPigeon.UseWaitCursor = true;
            this.pbPigeon.Click += new System.EventHandler(this.pbPigeon_Click);
            // 
            // txtCritterName
            // 
            this.txtCritterName.HideSelection = false;
            this.txtCritterName.Location = new System.Drawing.Point(158, 58);
            this.txtCritterName.Name = "txtCritterName";
            this.txtCritterName.Size = new System.Drawing.Size(183, 20);
            this.txtCritterName.TabIndex = 4;
            this.txtCritterName.UseWaitCursor = true;
            // 
            // pbCritterImages
            // 
            this.pbCritterImages.Enabled = false;
            this.pbCritterImages.Location = new System.Drawing.Point(160, 147);
            this.pbCritterImages.Name = "pbCritterImages";
            this.pbCritterImages.Size = new System.Drawing.Size(186, 156);
            this.pbCritterImages.TabIndex = 7;
            this.pbCritterImages.TabStop = false;
            this.pbCritterImages.UseWaitCursor = true;
            // 
            // lblCritterName
            // 
            this.lblCritterName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCritterName.Location = new System.Drawing.Point(172, 15);
            this.lblCritterName.Name = "lblCritterName";
            this.lblCritterName.Size = new System.Drawing.Size(165, 119);
            this.lblCritterName.TabIndex = 54;
            this.lblCritterName.Text = "label2";
            this.lblCritterName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCritterName.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Cornsilk;
            this.label1.Font = new System.Drawing.Font("Harlow Solid Italic", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightCoral;
            this.label1.Location = new System.Drawing.Point(217, 597);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 24);
            this.label1.TabIndex = 70;
            this.label1.Text = "Yas :";
            this.label1.UseWaitCursor = true;
            // 
            // lblCritter
            // 
            this.lblCritter.Font = new System.Drawing.Font("Harlow Solid Italic", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCritter.ForeColor = System.Drawing.Color.LightCoral;
            this.lblCritter.Location = new System.Drawing.Point(284, 599);
            this.lblCritter.Name = "lblCritter";
            this.lblCritter.Size = new System.Drawing.Size(34, 33);
            this.lblCritter.TabIndex = 23;
            this.lblCritter.Text = "3";
            this.lblCritter.UseWaitCursor = true;
            // 
            // j
            // 
            this.j.Location = new System.Drawing.Point(425, 112);
            this.j.Name = "j";
            this.j.Size = new System.Drawing.Size(30, 30);
            this.j.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.j.TabIndex = 0;
            this.j.TabStop = false;
            this.j.UseWaitCursor = true;
            // 
            // k
            // 
            this.k.Location = new System.Drawing.Point(467, 112);
            this.k.Name = "k";
            this.k.Size = new System.Drawing.Size(30, 30);
            this.k.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.k.TabIndex = 0;
            this.k.TabStop = false;
            this.k.UseWaitCursor = true;
            // 
            // pbMood
            // 
            this.pbMood.Location = new System.Drawing.Point(31, 111);
            this.pbMood.Name = "pbMood";
            this.pbMood.Size = new System.Drawing.Size(30, 30);
            this.pbMood.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMood.TabIndex = 0;
            this.pbMood.TabStop = false;
            this.pbMood.UseWaitCursor = true;
            // 
            // pbHunger
            // 
            this.pbHunger.Location = new System.Drawing.Point(77, 112);
            this.pbHunger.Name = "pbHunger";
            this.pbHunger.Size = new System.Drawing.Size(30, 30);
            this.pbHunger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbHunger.TabIndex = 0;
            this.pbHunger.TabStop = false;
            this.pbHunger.UseWaitCursor = true;
            // 
            // i
            // 
            this.i.Location = new System.Drawing.Point(382, 112);
            this.i.Name = "i";
            this.i.Size = new System.Drawing.Size(30, 30);
            this.i.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.i.TabIndex = 0;
            this.i.TabStop = false;
            this.i.UseWaitCursor = true;
            // 
            // pbSleep
            // 
            this.pbSleep.Location = new System.Drawing.Point(123, 112);
            this.pbSleep.Name = "pbSleep";
            this.pbSleep.Size = new System.Drawing.Size(30, 30);
            this.pbSleep.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSleep.TabIndex = 0;
            this.pbSleep.TabStop = false;
            this.pbSleep.UseWaitCursor = true;
            // 
            // h
            // 
            this.h.Location = new System.Drawing.Point(346, 112);
            this.h.Name = "h";
            this.h.Size = new System.Drawing.Size(30, 30);
            this.h.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.h.TabIndex = 0;
            this.h.TabStop = false;
            this.h.UseWaitCursor = true;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(200, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 23);
            this.label4.TabIndex = 22;
            this.label4.Text = "Yaşam Çizelgesi";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.UseWaitCursor = true;
            // 
            // d
            // 
            this.d.Location = new System.Drawing.Point(169, 112);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(30, 30);
            this.d.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.d.TabIndex = 0;
            this.d.TabStop = false;
            this.d.UseWaitCursor = true;
            // 
            // g
            // 
            this.g.Location = new System.Drawing.Point(302, 112);
            this.g.Name = "g";
            this.g.Size = new System.Drawing.Size(30, 30);
            this.g.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.g.TabIndex = 0;
            this.g.TabStop = false;
            this.g.UseWaitCursor = true;
            // 
            // e
            // 
            this.e.Location = new System.Drawing.Point(214, 112);
            this.e.Name = "e";
            this.e.Size = new System.Drawing.Size(30, 30);
            this.e.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.e.TabIndex = 0;
            this.e.TabStop = false;
            this.e.UseWaitCursor = true;
            // 
            // f
            // 
            this.f.Location = new System.Drawing.Point(259, 112);
            this.f.Name = "f";
            this.f.Size = new System.Drawing.Size(30, 30);
            this.f.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.f.TabIndex = 0;
            this.f.TabStop = false;
            this.f.UseWaitCursor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(14, 147);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(509, 23);
            this.progressBar1.TabIndex = 13;
            this.progressBar1.UseWaitCursor = true;
            // 
            // plPlay
            // 
            this.plPlay.BackColor = System.Drawing.Color.SkyBlue;
            this.plPlay.Controls.Add(this.btnOyunList);
            this.plPlay.Controls.Add(this.p16);
            this.plPlay.Controls.Add(this.pnlPlayList);
            this.plPlay.Controls.Add(this.label5);
            this.plPlay.Location = new System.Drawing.Point(0, 352);
            this.plPlay.Name = "plPlay";
            this.plPlay.Size = new System.Drawing.Size(261, 353);
            this.plPlay.TabIndex = 69;
            // 
            // btnOyunList
            // 
            this.btnOyunList.Location = new System.Drawing.Point(85, 220);
            this.btnOyunList.Name = "btnOyunList";
            this.btnOyunList.Size = new System.Drawing.Size(75, 23);
            this.btnOyunList.TabIndex = 22;
            this.btnOyunList.Text = "Oyna !!";
            this.btnOyunList.UseVisualStyleBackColor = true;
            this.btnOyunList.Click += new System.EventHandler(this.btnOyunList_Click_1);
            // 
            // p16
            // 
            this.p16.Image = ((System.Drawing.Image)(resources.GetObject("p16.Image")));
            this.p16.InitialImage = ((System.Drawing.Image)(resources.GetObject("p16.InitialImage")));
            this.p16.Location = new System.Drawing.Point(54, 87);
            this.p16.Name = "p16";
            this.p16.Size = new System.Drawing.Size(24, 24);
            this.p16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p16.TabIndex = 66;
            this.p16.TabStop = false;
            this.p16.UseWaitCursor = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pnlFriend
            // 
            this.pnlFriend.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlFriend.Controls.Add(this.lblCritterFriend);
            this.pnlFriend.Controls.Add(this.btnSpecial2);
            this.pnlFriend.Controls.Add(this.btnSpecial);
            this.pnlFriend.Controls.Add(this.pbCritterFriend);
            this.pnlFriend.Location = new System.Drawing.Point(1044, -3);
            this.pnlFriend.Name = "pnlFriend";
            this.pnlFriend.Size = new System.Drawing.Size(388, 708);
            this.pnlFriend.TabIndex = 68;
            // 
            // lblCritterFriend
            // 
            this.lblCritterFriend.Location = new System.Drawing.Point(110, 13);
            this.lblCritterFriend.Name = "lblCritterFriend";
            this.lblCritterFriend.Size = new System.Drawing.Size(186, 23);
            this.lblCritterFriend.TabIndex = 10;
            this.lblCritterFriend.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSpecial2
            // 
            this.btnSpecial2.Location = new System.Drawing.Point(110, 373);
            this.btnSpecial2.Name = "btnSpecial2";
            this.btnSpecial2.Size = new System.Drawing.Size(186, 60);
            this.btnSpecial2.TabIndex = 9;
            this.btnSpecial2.UseVisualStyleBackColor = true;
            // 
            // btnSpecial
            // 
            this.btnSpecial.Location = new System.Drawing.Point(110, 279);
            this.btnSpecial.Name = "btnSpecial";
            this.btnSpecial.Size = new System.Drawing.Size(186, 62);
            this.btnSpecial.TabIndex = 8;
            this.btnSpecial.UseVisualStyleBackColor = true;
            this.btnSpecial.Click += new System.EventHandler(this.btnSpecial_Click);
            // 
            // pbCritterFriend
            // 
            this.pbCritterFriend.Enabled = false;
            this.pbCritterFriend.Location = new System.Drawing.Point(133, 59);
            this.pbCritterFriend.Name = "pbCritterFriend";
            this.pbCritterFriend.Size = new System.Drawing.Size(140, 140);
            this.pbCritterFriend.TabIndex = 7;
            this.pbCritterFriend.TabStop = false;
            this.pbCritterFriend.UseWaitCursor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1430, 703);
            this.Controls.Add(this.pnlFriend);
            this.Controls.Add(this.plPlay);
            this.Controls.Add(this.plDiyalog);
            this.Controls.Add(this.plUyku);
            this.Controls.Add(this.PLYemek);
            this.Controls.Add(this.panel9);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.plDiyalog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.p15)).EndInit();
            this.plUyku.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.p13)).EndInit();
            this.PLYemek.ResumeLayout(false);
            this.pnlYemekList.ResumeLayout(false);
            this.pnlYemekList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).EndInit();
            this.pnlPlayList.ResumeLayout(false);
            this.pnlPlayList.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.pnlCritterInfo.ResumeLayout(false);
            this.pnlBornCritter.ResumeLayout(false);
            this.pnlBornCritter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPigeon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCritterImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.j)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.k)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHunger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.i)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSleep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.h)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.g)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.f)).EndInit();
            this.plPlay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.p16)).EndInit();
            this.pnlFriend.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCritterFriend)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel plDiyalog;
        private System.Windows.Forms.ComboBox cbConversation;
        private System.Windows.Forms.Label Talk_List;
        private System.Windows.Forms.PictureBox p15;
        private System.Windows.Forms.Panel plUyku;
        private System.Windows.Forms.PictureBox p13;
        private System.Windows.Forms.ComboBox combo_Sleep;
        private System.Windows.Forms.Panel PLYemek;
        private System.Windows.Forms.Panel pnlYemekList;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox p14;
        private System.Windows.Forms.Panel pnlPlayList;
        private System.Windows.Forms.RadioButton check_c;
        private System.Windows.Forms.RadioButton check_a;
        private System.Windows.Forms.RadioButton check_b;
        private System.Windows.Forms.RadioButton check_d;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pbCritterImages;
        private System.Windows.Forms.Label lblCritterName;
        private System.Windows.Forms.Panel pnlBornCritter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbDog;
        private System.Windows.Forms.Button btnBornCritter;
        private System.Windows.Forms.PictureBox pbFish;
        private System.Windows.Forms.PictureBox pbPigeon;
        private System.Windows.Forms.TextBox txtCritterName;
        private System.Windows.Forms.Label lblCritter;
        private System.Windows.Forms.PictureBox j;
        private System.Windows.Forms.PictureBox k;
        private System.Windows.Forms.PictureBox pbMood;
        private System.Windows.Forms.PictureBox pbHunger;
        private System.Windows.Forms.PictureBox i;
        private System.Windows.Forms.PictureBox pbSleep;
        private System.Windows.Forms.PictureBox h;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox d;
        private System.Windows.Forms.PictureBox g;
        private System.Windows.Forms.PictureBox e;
        private System.Windows.Forms.PictureBox f;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel plPlay;
        private System.Windows.Forms.PictureBox p16;
        private System.Windows.Forms.ListBox lbTalkHistory;
        private System.Windows.Forms.Button btnYemekList;
        private System.Windows.Forms.Button btnOyunList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlFriend;
        private System.Windows.Forms.Button btnSpecial2;
        private System.Windows.Forms.Button btnSpecial;
        private System.Windows.Forms.PictureBox pbCritterFriend;
        private System.Windows.Forms.Panel pnlCritterInfo;
        private System.Windows.Forms.Label lblCritterFriend;
    }
}

