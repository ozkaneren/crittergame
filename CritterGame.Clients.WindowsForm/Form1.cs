﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using CritterGame.Business;
using CritterGame.Business.Factory;
using CritterGame.Business.Helper;
using CritterGame.Clients.WindowsForm.DictionaryModel;

namespace CritterGame.Clients.WindowsForm
{
    public partial class Form1 : Form
    {
        private Critter _critter;
        private string _critterType;
        public int sayac = 0;
        bool chatMeaning = false;

        public Form1()
        {
            InitializeComponent();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            date = DateTime.Now; ;
            lblCritter.Text = "0";
            timer1.Interval = 5000;


            progressBar1.Maximum = 3650;
            progressBar1.Minimum = 0;
            progressBar1.Value = 3650;

            FillCritterImages();
            btnSpecial.Visible = false;
            btnSpecial2.Visible = false;

        }





        private void FillCritterImages()
        {
            pbDog.ImageLocation = @"C:\Users\ozkan.eren\Desktop\Gorseller\dog.png";
            pbDog.SizeMode = PictureBoxSizeMode.StretchImage;
            pbPigeon.ImageLocation = @"C:\Users\ozkan.eren\Desktop\Gorseller\pigeon.png";
            pbPigeon.SizeMode = PictureBoxSizeMode.StretchImage;
            pbFish.ImageLocation = @"C:\Users\ozkan.eren\Desktop\Gorseller\fish.png";
            pbFish.SizeMode = PictureBoxSizeMode.StretchImage;
          
        }



        private void SetActiveCritterImageStyle(PictureBox pb)
        {
            pb.BorderStyle = BorderStyle.Fixed3D;
            pb.BackColor = Color.Black;
        }

        private void SetPassiveCritterImageStyle(params PictureBox[] pbs)
        {
            foreach (PictureBox pb in pbs)
            {
                pb.BorderStyle = BorderStyle.None;
                pb.BackColor = Color.Transparent;
            }
        }

        private void BornCritter(string name)
        {
            _critter = CritterFactory.CreateCritter(name, _critterType);
          
            pnlBornCritter.Visible = false;
            
            pnlCritterInfo.Visible = true;

            pbCritterImages.ImageLocation = Dictionaries.CritterImages[_critterType];
            pbCritterImages.SizeMode = PictureBoxSizeMode.StretchImage;
            lblCritterName.Text = _critter.Name;

            //MessageBox.Show("Ben doğdum :)", $"{_critter.Name} konuşuyor...", MessageBoxButtons.OK,
            //    MessageBoxIcon.Information);


            RefreshAllStatusImages();
            timer1.Start();

        }

        private void pbPigeon_Click(object sender, EventArgs e)
        {
            _critterType = "Pigeon";

            SetActiveCritterImageStyle(sender as PictureBox);
            SetPassiveCritterImageStyle(pbFish, pbDog);
        }

        private void pbFish_Click(object sender, EventArgs e)
        {
            _critterType = "Fish";
            SetActiveCritterImageStyle(sender as PictureBox);
            SetPassiveCritterImageStyle(pbPigeon, pbDog);
        }

        private void pbDog_Click(object sender, EventArgs e)
        {
            _critterType = "Dog";
            SetActiveCritterImageStyle(sender as PictureBox);
            SetPassiveCritterImageStyle(pbFish, pbPigeon);
        }

        private void btnBornCritter_Click(object sender, EventArgs e)
        {


            string name = txtCritterName.Text;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Bana bir isim versene!", "Uyarı!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(_critterType))
            {
                MessageBox.Show("Bence bir türüm olmalı!", $"{name} konuşuyor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.BornCritter(name);

        }


        private void RefreshAllStatusImages()
        {

            ImagePrinter.HungerPrinter(pbHunger, _critter);
            ImagePrinter.MoodPrinter(pbMood, _critter);
            ImagePrinter.SleepPrinter(pbSleep, _critter);
        }

        private void combo_Talk_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListMoodWriter.Writer(lbTalkHistory, _critter);

            _critter.Talk(Dictionaries.ConversationStatusNumFinder[cbConversation.SelectedItem.ToString()]);
            _critter.Sleep(Dictionaries.ConversationStatusNumFinder[cbConversation.SelectedItem.ToString()]);
            _critter.Eat(Dictionaries.ConversationStatusNumFinder[cbConversation.SelectedItem.ToString()]);

            lbTalkHistory.Items.Add("");

            lbTalkHistory.Items.Add(Dictionaries.Conversation[cbConversation.Text]);

            if (cbConversation.SelectedItem.ToString() == "Naber ?")
            {
                lbTalkHistory.Items.Add($"{Dictionaries.StatusTranslater[_critter.CurrentMood.ToString()]}, öyle işte...");
            }

            lbTalkHistory.Items.Add("");


            int randomnum = RandomGenerator.Instance.GenerateRandomNumber(1, 8);

            //int randomnum = 6;
            lbTalkHistory.Items.Add(Dictionaries.RandomConversation[randomnum]);



            if (randomnum == 6)
            {
                chatMeaning = true;
            }


            int newcritterIdentifier = Convert.ToInt32(Dictionaries.TypeDeterminate[_critter.GetType()]);

            if (_critter.Age > 20 && chatMeaning)
            {
                _critter.FindFriend();
                pbCritterFriend.ImageLocation = Dictionaries.CritterImagesFriend[_critter.Friend.Type];
                pbCritterFriend.SizeMode = PictureBoxSizeMode.StretchImage;
                lblCritterFriend.Text = _critter.Friend.Name;
                btnSpecial.Text = Dictionaries.Feature[_critter.Friend.GetType()];
                btnSpecial.Visible = true;
                btnSpecial2.Visible = true;
                MessageBox.Show("Critter 'in yeni bir arkadaşa sahip :)");
              
            }
           
            RefreshAllStatusImages();

            chatMeaning = false;

        }


        private void btnYemekList_Click(object sender, EventArgs e)
        {
            int counter = 0; ;

            List<CheckBox> checkboxlist = new List<CheckBox>();
            checkboxlist.Add(pnlYemekList.Controls.OfType<CheckBox>().FirstOrDefault(c => c.Checked));
            foreach (CheckBox box in checkboxlist)
            {
                counter += box.TabIndex;
            }
            _critter.Eat(counter);
            RefreshAllStatusImages();
        }

        private void btnOyunList_Click_1(object sender, EventArgs e)
        {
            RadioButton counter = pnlPlayList.Controls.OfType<RadioButton>().FirstOrDefault(c => c.Checked);
            lbTalkHistory.Items.Add($"{_critter.Name} {counter.Text} oynadı");
            _critter.Play(counter.TabIndex);
            RefreshAllStatusImages();
        }

        private void combo_Sleep_SelectedIndexChanged(object sender, EventArgs e)
        {
            _critter.Sleep(Convert.ToInt32(combo_Sleep.SelectedItem));
            RefreshAllStatusImages();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            date = DateTime.Now;
            date = date.AddDays(sayac);

            sayac++;
            lblCritter.Text = sayac.ToString();
            _critter.GrowOld();
            RefreshAllStatusImages();
     
            progressBar1.Value--;

            if (sayac == 365)
            {
                MessageBox.Show("Doğum Günün Kutlu Olsuuun !!!");
            }

            if (progressBar1.Value == 0)
            {
                MessageBox.Show($"{_critter.Name} öldü :( .");
                Application.Restart();
                Application.Exit();
            }
            
        }

        private void btnSpecial_Click(object sender, EventArgs e)
        {
            if (_critter is Dog)
            {
                MessageBox.Show($"{_critter.Name} {_critter.Friend.Name} ile {((Dog) _critter).Run(5)}uyor :)");

            }
            else if (_critter is Pigeon)
            {
                MessageBox.Show($"{_critter.Name} {_critter.Friend.Name} ile {((Pigeon)_critter).Fly(5)}uyor :)");
            }
            else
            {
                MessageBox.Show($"{_critter.Name} {_critter.Friend.Name} ile {((Fish)_critter).Swim(5)}uyor :)");
            }
        }
    }
}
