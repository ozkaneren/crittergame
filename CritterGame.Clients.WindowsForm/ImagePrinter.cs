﻿using CritterGame.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CritterGame.Clients.WindowsForm.DictionaryModel;

namespace CritterGame.Clients.WindowsForm
{
    public static class ImagePrinter
    {
        public static void MoodPrinter(PictureBox pb, Critter critter)
        {
            pb.ImageLocation = Dictionaries.MoodImages[critter.CurrentMood];
            pb.SizeMode= PictureBoxSizeMode.StretchImage;
        }
        public static void SleepPrinter(PictureBox pb, Critter critter)
        {
            pb.ImageLocation = Dictionaries.SleepImages[critter.SleepStatus];
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        public static void HungerPrinter(PictureBox pb, Critter critter)
        {
            pb.ImageLocation = Dictionaries.EatImages[critter.HungerStatus];
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
        }
    }
}
