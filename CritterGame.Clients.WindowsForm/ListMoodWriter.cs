﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CritterGame.Business;
using CritterGame.Clients.WindowsForm.DictionaryModel;

namespace CritterGame.Clients.WindowsForm
{
    public static class ListMoodWriter
    {
        public static void Writer(ListBox lb ,Critter critter)
        {
            lb.Items.Clear();
            lb.Items.Add($"{critter.Name} diyor ki : ");
            lb.Items.Add($"{Dictionaries.StatusTranslater[critter.CurrentMood.ToString()]} ");
            lb.Items.Add($"{Dictionaries.StatusTranslater[critter.SleepStatus.ToString()]} ve ");
            lb.Items.Add($"{Dictionaries.StatusTranslater[critter.HungerStatus.ToString()]}.");

        }
    }
}
