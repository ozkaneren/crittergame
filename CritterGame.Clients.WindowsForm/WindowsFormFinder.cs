﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CritterGame.Clients.WindowsForm
{
    public class WindowsFormFinder
    {
        List<PictureBox> pictureboxlist = new List<PictureBox>();

        public void FindPictureBoxs(Panel panel)
        {
            foreach (Control c in panel.Controls)
                if (c.GetType() == typeof(PictureBox))
                {
                    pictureboxlist.Add((PictureBox)c);
                }
            WindowsFormOrder.PictureBoxListOrder(pictureboxlist);


        }


    }
}
