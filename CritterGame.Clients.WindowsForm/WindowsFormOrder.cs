﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CritterGame.Clients.WindowsForm
{
    public static class WindowsFormOrder
    {

       

        ///
        ///
        /// 
        /// 
        private static List<CheckBox> checkList;

        public static List<CheckBox> CheckList
        {
            get { return checkList; }
            set { checkList = value; }
        }

        public static void CheckBoxListOrder(List<CheckBox> cb)
        {
            CheckList = cb;
            CheckList = CheckList.OrderBy(p => p.Name).ToList();

        }


        ///
        /// 
        /// 
        /// 
        /// 
        ///
        private static List<PictureBox> pictureList;

        public static List<PictureBox> PictureList
        {
            get { return pictureList; }
            set { pictureList = value; }
        }

        public static void PictureBoxListOrder(List<PictureBox> pb)
        {
            pictureList = pb;
            pictureList = pictureList.OrderBy(p => p.Name).ToList();

        }

    }
}
